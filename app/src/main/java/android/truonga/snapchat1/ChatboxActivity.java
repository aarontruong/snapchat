package android.truonga.snapchat1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class ChatboxActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTitle("CHATBOX");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ChatboxFragment chatboxFrag = new ChatboxFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container, chatboxFrag).commit();
    }
}
