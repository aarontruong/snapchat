package android.truonga.snapchat1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class FriendRequestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTitle("Friend Requests");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FriendRequestFragment requestFragment = new FriendRequestFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container, requestFragment).commit();
    }

}
