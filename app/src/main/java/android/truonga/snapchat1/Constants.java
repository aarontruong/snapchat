package android.truonga.snapchat1;


public class Constants {

    public static final String ACTION_ADD_FRIEND = "com.example.snapchat1.ADD_FRIEND";
    public static final String ACTION_SEND_FRIEND_REQUEST = "com.example.snapchat1.SEND_FRIEND_REQUEST";
    public static final String ACTION_SEND_PHOTO = "com.example.snapchat1.SEND_PHOTO";


    public static final String BROADCAST_ADD_FRIEND_SUCCESS = "com.example.snapchat1.ADD_FRIEND_SUCCESS";
    public static final String BROADCAST_ADD_FRIEND_FAILURE = "com.example.snapchat1.ADD_FRIEND_FAILURE";

    public static final String BROADCAST_FRIEND_REQUEST_SUCCESS = "com.example.snapchat1.FRIEND_REQUEST_SUCCESS";
    public static final String BROADCAST_FRIEND_REQUEST_FAILURE = "com.example.snapchat1.FRIEND_REQUEST_FAILURE";

}
