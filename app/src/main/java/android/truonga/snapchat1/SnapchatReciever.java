package android.truonga.snapchat1;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class SnapchatReciever extends BroadcastReceiver {
    public SnapchatReciever() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
       String action = intent.getAction();
        if(action.equals(Constants.BROADCAST_ADD_FRIEND_SUCCESS)){
            Toast.makeText(context, "Added Friend!", Toast.LENGTH_SHORT).show();
        }else if(action.equals(Constants.BROADCAST_ADD_FRIEND_FAILURE)){
            Toast.makeText(context, "Failed to add friend", Toast.LENGTH_SHORT).show();
        }else if(action.equals(Constants.BROADCAST_FRIEND_REQUEST_SUCCESS)) {
            Toast.makeText(context, "Friend Request sent!", Toast.LENGTH_SHORT).show();

        }else if(action.equals(Constants.BROADCAST_ADD_FRIEND_FAILURE)) {
            Toast.makeText(context, "Failed to send request!", Toast.LENGTH_SHORT).show();

        }
    }
}
