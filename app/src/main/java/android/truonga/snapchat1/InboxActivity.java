package android.truonga.snapchat1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class InboxActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTitle("INBOX");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        InboxFragment inboxFrag = new InboxFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container, inboxFrag).commit();
    }
}
