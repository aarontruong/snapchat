package android.truonga.snapchat1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class FriendsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTitle("FRIENDS");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FriendsFragment friendsFrag = new FriendsFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container, friendsFrag).commit();
    }
}
