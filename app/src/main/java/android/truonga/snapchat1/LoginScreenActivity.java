package android.truonga.snapchat1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class LoginScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LoginScreenFragment loginScreenFrag= new LoginScreenFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container, loginScreenFrag).commit();
    }

}
