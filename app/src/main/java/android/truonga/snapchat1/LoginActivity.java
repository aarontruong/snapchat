package android.truonga.snapchat1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.backendless.Backendless;

public class LoginActivity extends AppCompatActivity {

    public static final String APP_ID="1EA89598-E4A9-9E6E-FF08-EA53AD961800";
    public static final String SECRET_KEY="696E2A7F-712E-0F20-FF91-3166B6C35B00";
    public static final String VERSION="v1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTitle("Snapchat: LOGIN");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Backendless.initApp(this, APP_ID, SECRET_KEY, VERSION);
        if(Backendless.UserService.loggedInUser() == ""){
            LoginActivityFragment loginFragment = new LoginActivityFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container, loginFragment).commit();
        } else {
            CategoryFragment categoryFragment = new CategoryFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container, categoryFragment).commit();

        }

    }
}
