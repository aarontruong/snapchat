package android.truonga.snapchat1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

public class RegisterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTitle("REGISTER");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RegisterFragment registerFrag = new RegisterFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container, registerFrag).commit();

    }
}
