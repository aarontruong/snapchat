package android.truonga.snapchat1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class CameraActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTitle("CAMERA");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        CameraFragment cameraFrag = new CameraFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container, cameraFrag).commit();
    }
}
