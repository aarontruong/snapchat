package android.truonga.snapchat1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SendPictureActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTitle("SEND A PICTURE");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SendPictureFragment sendPicFrag = new SendPictureFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container, sendPicFrag).commit();
    }
}
