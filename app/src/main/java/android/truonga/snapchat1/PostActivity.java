package android.truonga.snapchat1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class PostActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        PostFragment postFrag = new PostFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container, postFrag).commit();
    }
}
