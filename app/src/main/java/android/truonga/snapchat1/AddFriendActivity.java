package android.truonga.snapchat1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class AddFriendActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTitle("ADD A FRIEND");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AddFriendFragment addFriend= new AddFriendFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container, addFriend).commit();
    }
}
