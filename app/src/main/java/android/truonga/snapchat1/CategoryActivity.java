package android.truonga.snapchat1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class CategoryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTitle("Categories");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        CategoryFragment categoryFrag = new CategoryFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container, categoryFrag).commit();
    }
}
